DEBUG = false;

numParticipants = 64;
numNodes = (numParticipants * 2) - 1;
numNodesCreated = 0;
maxDepth = Math.ceil(log2(numParticipants));

nodeWidth = 100;
nodeHeight = 20;

horizontalNodeSpacing = 10;
verticalNodeSpacing = 0;

treeWidth = ((nodeWidth + horizontalNodeSpacing) * (maxDepth + 1)) - horizontalNodeSpacing;
treeHeight = nodeHeight * numNodes;
treeCenter = (numParticipants - 1) * nodeHeight;

nodeOffsets = [];

mouseStartX = 0;
mouseStartY = 0;
isPanning = false;

$(document).ready(function(){
    canvas = document.getElementById("tree");
    if( canvas != null )
    {
        ctx = canvas.getContext("2d");
        ctx.lineWidth = 1;
        ctx.strokeStyle = "#000000";
        generateTree();
        bindEvents();
    }
});

function bindEvents()
{
    $('#treewrap').mousedown(panStart);
    $('#treewrap').mousemove(pan);
    $('#treewrap').hover(function(){$("#tree").css("cursor", "pointer")}, panEnd);
    $('#treewrap').mouseout(panEnd);
    $('#treewrap').mouseup(panEnd);
}

function panStart(eventData)
{
    mouseStartX = eventData.pageX;
    mouseStartY = eventData.pageY;
    initialOffset = $("#tree").offset();
    $('body').each(function() {           
        $(this).attr('unselectable', 'on')
               .css({
                   '-ms-user-select':'none',
                   '-moz-user-select':'none',
                   '-webkit-user-select':'none',
                   'user-select':'none'
               })
               .each(function() {
                   this.onselectstart = function() { return false; };
               });
    });
    isPanning = true;
}

function pan(eventData)
{
    if(isPanning)
    {
        var deltaX = eventData.pageX - mouseStartX;
        var deltaY = eventData.pageY - mouseStartY;
        $("#tree").css("cursor", "move");
        $("#tree").offset({top:deltaY + initialOffset.top, left:deltaX + initialOffset.left});
    }
}

function panEnd()
{
    $("#tree").css("cursor", "pointer");
    $('body').each(function() {           
        $(this).attr('unselectable', 'off')
               .css({
                   '-ms-user-select':'',
                   '-moz-user-select':'',
                   '-webkit-user-select':'',
                   'user-select':''
               })
    });
    isPanning = false;
    mouseStartY = $("#tree").offset().top;
    mouseStartX = $("#tree").offset().left;
}

function generateTree()
{
    canvas.width = treeWidth;
    canvas.height = treeHeight;
    drawRounds();
    createNode();
}

function drawRounds()
{
    ctx.fillStyle = "rgb(200,200,200)";
    for(var i = 0; i <= maxDepth; i++)
    {
        ctx.fillRect((i * (nodeWidth + horizontalNodeSpacing)) - Math.round(horizontalNodeSpacing / 4), 0, nodeWidth + Math.round(horizontalNodeSpacing / 2), treeHeight);
    }
    ctx.fillStyle = "rgb(200,0,0)";
    
}

function createNode(parentNode, direction)
{
    var currentNode = {x: 0, y: 0, depth: 0};
    if (parentNode === undefined)
    {
        currentNode.x = treeWidth - nodeWidth;
        currentNode.y = treeCenter;
        currentNode.depth = 0; 
    }
    else
    { 
        currentNode.x = parentNode.x - nodeWidth - horizontalNodeSpacing;
        currentNode.depth = parentNode.depth + 1;
        var offset = 0;
      
        if(parentNode.depth === 0)
           offset = 38 * nodeHeight;
        if(parentNode.depth === 1)
           offset = 21 * nodeHeight;
        if(parentNode.depth === 2)
           offset = 12 * nodeHeight;
        if(parentNode.depth === 3)
           offset = 7 * nodeHeight;
        //offset = getOffset(currentNode.depth);
      
        currentNode.y = parentNode.y + (direction * (((maxDepth - currentNode.depth+1) * nodeHeight) - offset));        
    }
    numNodesCreated++;
    drawNode(currentNode, parentNode);

    

    for (var i = 0; i < 2; i++)
    {
        if (numNodesCreated < numNodes && currentNode.depth < maxDepth)
        {
            var childDirection = 1;
            if (i == 1)
                childDirection = -1;
            createNode(currentNode, childDirection);
        } 
    }
}

function drawNode(node, parentNode)
{
    if(parentNode !== undefined)
    {
        ctx.beginPath();  
        ctx.moveTo(node.x, node.y + (Math.round(nodeHeight/2)+0.5));
        ctx.lineTo(node.x + (Math.round(horizontalNodeSpacing/2)+0.5) + nodeWidth, node.y + (Math.round(nodeHeight/2)+0.5));
        ctx.lineTo(node.x + (Math.round(horizontalNodeSpacing/2)+0.5) + nodeWidth, parentNode.y + (Math.round(nodeHeight/2)+0.5));
        ctx.lineTo(parentNode.x, parentNode.y + (Math.round(nodeHeight/2)+0.5)); 
        ctx.stroke();         
    }
    ctx.fillRect(node.x, node.y , nodeWidth, nodeHeight);
    if(DEBUG)
    {
        console.log(mul_str("  ", node.depth)+"drawing node: "+numNodesCreated+"/"+numNodes + " depth: "+node.depth+"/"+maxDepth + " pos: ["+node.x+","+node.y+"] ("+(maxDepth - node.depth+1)+")");
    }
    
}

function getOffset(currentDepth)
{
    
    if(maxDepth > 2)
    {
        for(var i = currentDepth; i < maxDepth - 1; i++)
        {
            console.log("depth: "+currentDepth);
            
        }
        //return (Math.pow(2, maxDepth - 2) + 1) * nodeHeight;
    }
}

function log2(n)
{
    return Math.log(n) / Math.LN2;
  
}

function mul_str(str, num) {
    if (!num) return "";
    var newStr = str;
    while (--num) newStr += str;
    return newStr;
}