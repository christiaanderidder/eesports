class Tournament::Participation < ActiveRecord::Base
  belongs_to :tournament
  belongs_to :user
end
