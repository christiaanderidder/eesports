class Tournament::Match < ActiveRecord::Base
  belongs_to :tournament
  belongs_to :participant_1, :class_name => 'TournamentParticipant'
  belongs_to :participant_2, :class_name => 'TournamentParticipant'
  belongs_to :winner, :class_name => 'TournamentParticipant'
end
