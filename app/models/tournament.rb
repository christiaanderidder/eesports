class Tournament < ActiveRecord::Base
  belongs_to :game
  belongs_to :event
  has_many :tournament_participations, :class_name => 'Tournament::Participation'
  has_many :users, :through => :tournament_participations
  has_many :matches

  def open?
    #check if tournament is open
    return (self.signup_opens_at < Time.now and self.signup_closes_at > Time.now and
            self.users.count < self.max_participants)  
  end
end
