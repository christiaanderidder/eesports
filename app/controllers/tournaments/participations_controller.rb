class Tournaments::ParticipationsController < ApplicationController
  before_filter :authenticate_user!

  def create
    @tournament = Tournament.find(params[:tournament_id])
    @participation = Tournament::Participation.find_by_tournament_id_and_user_id(params[:tournament_id], current_user.id)

    if @participation.present?
      redirect_to(tournament_url(params[:tournament_id]), :flash => {:error => 'You already signed up'} )
    elsif !@tournament.open?
      redirect_to(tournament_url(params[:tournament_id]), :flash => {:error => 'This tournament is not open for sign up'} )
    else
      @participation = Tournament::Participation.new(:tournament => @tournament, :user_id => current_user.id)     
      if @participation.save
        redirect_to(tournament_url(params[:tournament_id]), :flash => {:success => 'You successfully signed up'} )
      else
        redirect_to(tournament_url(params[:tournament_id]), :flash => {:error => 'An error occured while signing up'} )
      end
    end
  end

  def destroy
    @tournament = Tournament.find(params[:id])
    @tournament.destroy
    redirect_to tournaments_url
  end

  def signup
    @tournament = Tournament.find(params[:id])
  end

end
