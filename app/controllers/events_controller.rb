class EventsController < ApplicationController
  def index
    @events = Event.all
    @event = Event.first
  end

  def show
    @event = Event.find(params[:id])
  end

  def new
    @event = Event.new
  end

  def edit
    @event = Event.find(params[:id])
  end

  def create
    @event = Event.new(params[:event])
    if @event.save
      redirect_to(edit_event_url(@event), :flash => {:success => 'Event was successfully created.'} )
    else
      render(:action => 'new')
    end
  end

  def update
    @event = Event.find(params[:id])
    if @event.update_attributes(params[:event])
      redirect_to(edit_event_url(@event), :flash => {:success => 'Event was successfully updated.'} )
    else
      render(:action => 'edit')
    end
  end

  def destroy
    @event = Event.find(params[:id])
    @event.destroy
    redirect_to events_url
  end

end