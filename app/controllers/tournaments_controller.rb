class TournamentsController < ApplicationController
  def index
    @tournaments = Tournament.all
    @participations = Tournament::Participation.all
  end

  def show
    @tournament = Tournament.find(params[:id])
  end

  def new
    @tournament = Tournament.new
  end

  def edit
    @tournament = Tournament.find(params[:id])
  end

  def create
    @tournament = Tournament.new(params[:tournament])
    if @tournament.save
      redirect_to(edit_tournament_url(@tournament), :flash => {:success => 'Tournament was successfully created.'} )
    else
      render(:action => 'new')
    end
  end

  def update
    @tournament = Tournament.find(params[:id])
    if @tournament.update_attributes(params[:tournament])
      redirect_to(edit_tournament_url(@tournament), :flash => {:success => 'Tournament was successfully updated.'} )
    else
      render(:action => 'edit')
    end
  end

  def destroy
    @tournament = Tournament.find(params[:id])
    @tournament.destroy
    redirect_to tournaments_url
  end

end