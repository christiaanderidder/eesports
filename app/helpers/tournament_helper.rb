module TournamentHelper
  def generate_bracket(t)
    table_html = "<table>\n"

    table_rows = t.max_participants
    table_cols = Math.log(t.max_participants, 2).ceil
    
    logger.debug "Rows: #{table_rows}\nCols: #{table_cols}\n"
    
    1.upto(table_rows) do |row_num|
      table_html << "  <tr class=\"r#{row_num}\">\n"
      1.upto(table_cols) do |col_num|
        matches_num = (table_rows.to_f**(1/col_num.to_f)).ceil
        if row_num <= matches_num
          table_html << "    <td class=\"c#{col_num}\" style=\"background: #f00\">Match</td>\n"
        else
          table_html << "    <td class=\"c#{col_num}\" >x</td>\n"
        end
      end

      table_html << "  </tr>\n"

    end
    table_html << "</table>\n"
  end
end