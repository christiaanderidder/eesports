# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120131160532) do

  create_table "events", :force => true do |t|
    t.string   "title"
    t.text     "body"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "games", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "image_url"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "posts", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "tournament_matches", :force => true do |t|
    t.integer  "round"
    t.integer  "match"
    t.integer  "participation_1_id"
    t.integer  "participation_2_id"
    t.integer  "winner_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "tournament"
  end

  add_index "tournament_matches", ["participation_1_id"], :name => "index_tournament_matches_on_participation_1_id"
  add_index "tournament_matches", ["participation_2_id"], :name => "index_tournament_matches_on_participation_2_id"
  add_index "tournament_matches", ["winner_id"], :name => "index_tournament_matches_on_winner_id"

  create_table "tournament_participations", :force => true do |t|
    t.integer  "tournament_id"
    t.integer  "user_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "tournament_participations", ["tournament_id"], :name => "index_tournament_participations_on_tournament_id"
  add_index "tournament_participations", ["user_id"], :name => "index_tournament_participations_on_user_id"

  create_table "tournaments", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "signup_opens_at"
    t.datetime "signup_closes_at"
    t.integer  "max_participants"
    t.integer  "min_participants"
    t.float    "participation_fee"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.integer  "event_id"
    t.integer  "game_id"
  end

  create_table "users", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
