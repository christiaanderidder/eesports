class CreateTournamentParticipations < ActiveRecord::Migration
  def change
    create_table :tournament_participations do |t|
      t.references :tournament
      t.references :user

      t.timestamps
    end
    add_index :tournament_participations, :tournament_id
    add_index :tournament_participations, :user_id
  end
end
