class CreateTournamentMatches < ActiveRecord::Migration
  def change
    create_table :tournament_matches do |t|
      t.integer :round
      t.integer :match
      t.integer :participation_1_id
      t.integer :participation_2_id
      t.integer :winner_id

      t.timestamps
    end
    add_index :tournament_matches, :participation_1_id
    add_index :tournament_matches, :participation_2_id
    add_index :tournament_matches, :winner_id
  end
end
