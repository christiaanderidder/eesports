class CreateTournaments < ActiveRecord::Migration
  def change
    create_table :tournaments do |t|
      t.string :title
      t.text :description
      t.datetime :signup_opens_at
      t.datetime :signup_closes_at
      t.integer :max_participants
      t.integer :min_participants
      t.float :participation_fee
      t.timestamps
      t.references :event
      t.references :game
    end
  end
end
